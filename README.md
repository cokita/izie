# izie

<p align="center">
<img src="https://laravel.com/assets/img/components/logo-laravel.svg"> &nbsp;
<img alt="@angular" class="TableObject-item avatar" height="100" itemprop="image" src="https://avatars2.githubusercontent.com/u/139426?v=3&amp;s=100" width="100"> &nbsp;
<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/d/d1/Ionic_Logo.svg/250px-Ionic_Logo.svg.png" width="250" alt="Ionic Framework">
</p>

Dificuldades/Desafios: 

1) Sem monitor extra em um notebook de 13";
2) Demorei para achar a documentação do IONIC na versão 1;
3) Hosts locais na API. Utilizando o Genymotion como máquina virutal, ele nào reconhecia o meu IP como do APACHE,
  descobri que podeiria utilizar o IP da máquina virual criada.
4) Montar uma arquitetura do zero em pouco tempo;
5) Utilizar o Eloquent como ORM, quando implementamos o Laravel, decidimos utilizar o PROPEL 2 e com isso não tinha nenhuma experiência com Eloquent. Não podia perder tempo implementando o PROPEL 2.
6) TEMPO! Tinha que terminar nesse feriado, pois trabalho em período integral e a noite além de ficar muito corrido não sairia com a qualidade que eu desejara, pois já estaria cansada depois do dia de trabalho.


  
Para confirgurar o ambiente:
 
    1) Crie um VHOST em seu apache com a URL: http://izie.api.com/. Aponte para: izie/api/public.
    2) Se não existir o arquivo .env, copie o .env.exemple e renomeie para .env
    3) Gere a key do Laravel (na raiz da api, execute): php artisan key:generate
    4) Dentro da pasta: izie/api/database/scripts, tem um arquivo chamado Scripts.sql. Para gerar o banco.
    5) Depois de criar o banco, mude as configurações de conexão em: izie/api/app/database.php e em .env
    6) Dar permissões nas pastas: storage e bootstrap
    7) No izie/api rodar composer update, depois composer dump-autoload
    
    8) Em app, rode bower install para instalar os componentes bower
    9) Em app, rode npm install
    
    
<h3>Estratégia</h3>
    
    Para ilustrar como faria na vida real, criei releases da aplicação, em suas versões 1, 2 e 3.
    
    Porém para facilitar o desenvolvimento, a visualização (sem precisar baixar as 3 versões) e 
    teste eu criei TRÊS aplicativos diferentes.   
    
OBS: Existe uma pasta de front que nela tem uma arquitetura em AngularJs que antes de receber a prova estava montando, pois achei que seria web e não APP. 

<h3>Obrigada pela oportunidade!</h3>
<p>Atenciosamente, Ana Flávia Carvalho</p>

<h4>Tel: 61 - 9 9618 - 0178</h4>
<h4>Email: cokitabr@gmail.com</h4>
    
     
