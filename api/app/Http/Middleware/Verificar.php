<?php

namespace App\Http\Middleware;

use Closure;

class Verificar
{
    private $rotasPorUrl = array(
        '/',
        '/401'
        );
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //Verifico se a requisição está vindo de URL do browser 
        $rota = $request->getRequestUri();  
        if (in_array($rota, $this->rotasPorUrl)) {
            return $next($request);
        } else if (($request->ajax() || $request->isJson() || $request->wantsJson())
           || strstr($request->header('Content-type'), 'multipart/form-data;')) 
        {

        }else{
            //return response()->json(['error'=> 1, 'mensagem' => 'Acesso Negado!'], 401);
            return redirect('/401');
        }

        return $next($request);
    }

}