<?php

namespace App\Http\Services\V2;

class ClienteService extends \App\Http\Services\ClienteService
{
    public function __construtor()
    {
    }

    public function validarClienteV2()
    {
        $this->validarCliente();
        if (empty($this->cliente->CPF))
            throw new \Exception("O CPF do cliente é obrigatório");

        if (empty($this->cliente->Email))
            throw new \Exception("A Email do cliente é obrigatório");

    }
}
