<?php

namespace App\Http\Services;
use App\Models\Endereco;
use App\Models\Uf;

class EnderecoService extends Service
{
    /** @var Endereco $this->endereco */
    protected $endereco;

    public function __construtor()
    {

    }

    public function removerEndereco($id)
    {
        /** @var Endereco $endereco */
        $endereco = Endereco::Where('Ativo', '=', '1')->find($id);
        if(!$endereco)
            throw new \Exception("Nenhum endereço encontrado com esse Id.");

        $endereco->Ativo = 0;
        $endereco->save();
        return $endereco;
    }

    public function recuperarEnderecos($idCliente)
    {
        /** @var Endereco $enderecos */
        $enderecos = Endereco::where('Ativo', '=', 1)
                ->where('IdCliente', '=', $idCliente)
                ->OrderByDesc('DataCriacao')->get();
        return $enderecos;
    }

    public function setEndereco($params)
    {
        $this->endereco = new Endereco($params);

        if (isset($params['Id']) && $params['Id'] != "") {
            $endereco = Endereco::find($params['Id']);
            if ($endereco) {
                $this->endereco = $endereco->fill($params);
            }
        }

        return $this->endereco;
    }

    public function validarEndereco()
    {
        if (!$this->endereco)
            throw new \Exception("Favor carregar as informações no objeto endereço");

        if (empty($this->endereco->Nome))
            throw new \Exception("O nome (identificador) do endereço obrigatório");

        if (empty($this->endereco->CEP))
            throw new \Exception("O CEP é obrigatória");

        if (empty($this->endereco->Logradouro))
            throw new \Exception("O logradouro é obrigatório");

        if (empty($this->endereco->IdUf))
            throw new \Exception("A UF é obrigatória");

        if (empty($this->endereco->Bairro))
            throw new \Exception("O bairro é obrigatório");

        if (empty($this->endereco->Cidade))
            throw new \Exception("A cidade é obrigatória");

    }

    public function salvar()
    {
        try {
            if(!$this->endereco->IdCliente)
                throw new \Exception("Favor informar para qual cliente deseja cadastrar um endereço");

            $this->endereco->save();
            return $this->endereco;
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }
}
