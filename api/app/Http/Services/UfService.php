<?php

namespace App\Http\Services;
use App\Http\Services\Service;
use App\Models\Uf;

class UfService extends Service
{
    public function __construtor()
    {

    }

    public function recuperarTodasUfs()
    {
    	$ufs = Uf::where('Ativo', '=', 1)->orderBy('Nome')->get()->toArray();
    	return $ufs;
    }
}
