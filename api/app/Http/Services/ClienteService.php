<?php

namespace App\Http\Services;

use App\Http\Services\Service;
use App\Models\Cliente;
use App\Models\Endereco;

class ClienteService extends Service
{
    /**
     * @var Cliente $this->cliente
     */
    public $cliente = null;

    public function __construtor()
    {
    }

    public function consultar()
    {
        return Cliente::Where('Ativo', '=', 1)->OrderByDesc('DataCriacao')->get();
    }

    public function removerClientePorId($id)
    {
        try {
            /** @var Cliente $cliente */
            $cliente = Cliente::Where('Ativo', '=', 1)->find($id);
            if (!$cliente)
                throw new \Exception("Nenhum cliente encontrado com esse ID");

            $cliente->__set('Ativo', 0);
            $cliente->save();

            return $cliente;
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function consultarClientePorId($id)
    {
        $id = intval($id);
        $cliente = Cliente::with('Imagem')->find($id);
        $enderecos = Endereco::with('Uf')
            ->Where('Ativo', '=', 1)
            ->Where('IdCliente', '=', $id)
            ->get();
        $cliente->__set('enderecos',  $enderecos);

        $imagemService = new ImagemService();
        if ($cliente->imagem && !$cliente->imagem->Base64) {
            $imagemBase64 = $imagemService->imageToBase64($cliente->imagem->Path . $cliente->imagem->NomeArquivado);
            $cliente->imagem->__set('Base64', $imagemBase64);
        } else if ($cliente->imagem && $cliente->imagem->Base64) {
            $cliente->imagem->__set('Base64', $cliente->imagem->Base64);
        }

        return $cliente;
    }

    public function setCliente($params)
    {
        if(!empty($params['DataNascimento'])) {
            $dataNasc = new \DateTime($params['DataNascimento']);
            $params['DataNascimento'] = $dataNasc->format('Y-m-d');
        }

        $this->cliente = new Cliente($params);
        if (isset($params['Id']) && $params['Id'] != "") {
            $cliente = Cliente::find($params['Id']);
            if ($cliente) {
                $this->cliente = $cliente->fill($params);
            }
        }

        if(!empty($params['enderecos'])){
            $enderecoService = new EnderecoService();
            foreach ($params['enderecos'] as $endereco){
                $enderecoModel = $enderecoService->setEndereco($endereco);
                $enderecoService->validarEndereco();
                $this->cliente->enderecos->add($enderecoModel);
                //$this->cliente->push();
            }
        }
    }


    public function salvar($idImagem = null)
    {
        try {
            if ($idImagem)
                $this->cliente->__set('IdImagem', $idImagem);

            $this->cliente->save();

            if($this->cliente->enderecos->isNotEmpty()){
                $this->cliente->enderecos()->saveMany($this->cliente->enderecos);
            }

            return $this->cliente;
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function validarCliente()
    {
        if (!$this->cliente)
            throw new \Exception("Favor carregar as informações no objeto cliente");

        if (empty($this->cliente->Nome))
            throw new \Exception("O nome do cliente é obrigatório");

        if (empty($this->cliente->DataNascimento))
            throw new \Exception("A data de nascimento do cliente é obrigatória");

        if (empty($this->cliente->Sexo))
            throw new \Exception("O sexo do cliente é obrigatório");

    }
}
