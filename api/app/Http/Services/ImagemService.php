<?php

namespace App\Http\Services;

use App\Models\Imagem;

class ImagemService extends Service
{
    public $pathToSave;

    public function __construct()
    {
        //parent::__construct();

        //$this->pathToSave = public_path()."/upload/";
    }

    /**
     * Método responsável em salvar a imgem em banco
     * Tinha feito salvando em arquivo, mas achei melhor deixar funcionando com o banco
     * @param $base64File
     * @return Imagem
     * @throws \Exception
     */
    public function salvar($base64File)
    {
        try {
            $this->pathToSave = public_path()."/upload/";
            //Para salvar em ARQUIVO
            //$result = $this->saveBase64Image($base64File);
            $finfo = $this->retornarInfoImagem($base64File);
            $imagem = new Imagem();
            $imagem->NomeArquivado = md5(uniqid(rand(), true)).'.'.$finfo['ext']; //$result['file'];
            $imagem->Path = null;//$this->pathToSave;
            $imagem->Base64 = $base64File;
            $imagem->Mimetype = $finfo['mimetype'];
            $imagem->save();
            return $imagem;
        }catch (\Exception $e){
            throw new \Exception($e->getMessage());
        }

    }

    /**
     * Quando salvando em arquivo, essa função serve para retornar o Bas64 a partir do path.
     * @param $path
     * @return string
     */
    public function imageToBase64($path)
    {
        $type = pathinfo($path, PATHINFO_EXTENSION);
        $data = file_get_contents($path);
        return 'data:image/' . $type . ';base64,' . base64_encode($data);

    }

    /**
     * Salva um arquivo em diretório
     * @param $base64
     * @return array
     */
    public function saveBase64Image($base64)
    {

        $nome_arquivo = md5(uniqid(rand(), true));
        $finfo = $this->retornarInfoImagem($base64);
        $nome_arquivo .= $nome_arquivo . '.' . $finfo['ext'];

        file_put_contents($this->pathToSave . $nome_arquivo, base64_decode($finfo['data']));
        return array('file' => $nome_arquivo, 'mimetype' => $finfo['mimetype']);
    }

    /**
     * Pelo base64, retorna as informações do arquivo.
     * @param $base64
     * @return array
     */
    private function retornarInfoImagem($base64)
    {
        $info = array();
        $splited = explode(',', substr($base64, 5), 2);
        $mime = $splited[0];
        $data = $splited[1];
        $info['data'] = $data;
        $mime_split_without_base64 = explode(';', $mime, 2);
        $info['mimetype'] = $mime_split_without_base64[0];
        $mime_split = explode('/', $mime_split_without_base64[0], 2);
        if (count($mime_split) == 2) {
            $extension = $mime_split[1];
            if ($extension == 'jpeg') $extension = 'jpg';

            $info['ext'] = $extension;
        }

        return $info;
    }
}
