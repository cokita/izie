<?php

namespace App\Http\Services\V3;

class ClienteService extends \App\Http\Services\ClienteService
{
    public function __construtor()
    {
    }

    public function validarClienteV3()
    {
        $this->validarCliente();
        if (empty($this->cliente->CPF))
            throw new \Exception("O CPF do cliente é obrigatório");

    }
}
