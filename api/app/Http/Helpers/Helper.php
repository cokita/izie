<?php // Code within app\Helpers\Helper.php

namespace App\Helpers;

class Helper
{
    /**
     * Método para aplicar mascara no valor informado
     *
     * @param $valor
     * @param $mascara
     * @return mixed
     */
    public static function mascara($valor, $mascara)
    {
        if ($valor) {
            $str = str_replace(" ", "", $valor);

            for ($i = 0; $i < strlen($str); $i++) {
                $mascara[strpos($mascara, "#")] = $str[$i];
            }

            return $mascara;
        }

        return '';
    }

    /**
     * Método para formatar a data para o formato BR
     *
     * @param $valor
     * @param $mascara
     * @return mixed
     */
    public static function dataBr($data)
    {
        $date = new \DateTime($data);
        return $date->format('d/m/Y');
    }

    /**
     * Método para adicionar dias à data informada
     *
     * @param $data
     * @param $dias
     * @return false|string
     */
    public static function dataSomaDias($data,$dias)
    {
        $data = date('d/m/Y', strtotime("+$dias days",strtotime($data)));
        return $data;
    }

    /**
     * Método para retornar o dia, nome do mês e o ano
     *
     * @return array
     */
    public static function dataDiaMesAno($data)
    {
        $date = new \DateTime($data);
        $arrMeses = ['01' => 'Janeiro', '02' => 'Fevereiro', '03' => 'Março', '04' => 'Abril',
                     '05' => 'Maio', '06' => 'Junho', '07' => 'Julho', '08' => 'Agosto',
                     '09' => 'Setembro', '10' => 'Outubro', '11' => 'Novembro', '12' => 'Dezembro'];
        $data = explode('/',$date->format('d/m/Y'));
        $dataFormatada = array($data[0],$arrMeses[$data[1]],$data[2]);

        return $dataFormatada;
    }

    /**
     * Método para formatar o valor para moeda brasileira
     *
     * @param $valor
     * @return string
     */
    public static function moedaBr($valor)
    {
        setlocale(LC_MONETARY,"pt_BR", "ptb");
        return money_format('%n', $valor);
    }

    /**
     * Metodo para formatar o valor em percentual no formato brasileiro
     *
     * @param $valor
     * @return mixed
     */
    public static function percentualBr($valor)
    {
        return str_replace('.', ',', $valor);
    }

    /**
     * Retorna o base64 de um arquivo
     *
     * @param $path
     */
    public static function arquivoParaBase64($path){
        $type = pathinfo($path, PATHINFO_EXTENSION);
        $data = file_get_contents($path);
        $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
        return $base64;
    }
}