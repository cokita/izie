<?php

namespace App\Http\Controllers\Core;
use App\Http\Controllers\Controller;
use App\Http\Services\UfService;

use App\Http\Retornos\RetornoPadrao;


class UfController extends Controller
{
    protected $ufService;

    /**
     * ClienteController constructor.
     */
    public function __construct()
    {
        $this->ufService = new UfService();

    }

    public function consultarTodas()
    {
        $retorno = new RetornoPadrao();
        try {
            $ufs = $this->ufService->recuperarTodasUfs();
            $retorno->setData($ufs);            
            return $this->ResponseCustom($retorno->getObjeto());

        } catch (\Exception $e) {
            $retorno->setErro(1);
            $retorno->setMensagem($e->getMessage());
            return $this->ResponseCustom($retorno->getObjeto(), 400);
        }
    }

}