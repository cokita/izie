<?php

namespace App\Http\Controllers\Core;
use App\Http\Controllers\Controller;
use App\Http\Retornos\RetornoPadrao;
use App\Http\Services\EnderecoService;
use Illuminate\Http\Request;


class EnderecoController extends Controller
{
    protected $enderecoService;

    /**
     * ClienteController constructor.
     */
    public function __construct()
    {
        $this->enderecoService = new EnderecoService();

    }

    public function consultarEnderecosPorCliente($idCliente)
    {
        $retorno = new RetornoPadrao();
        try {
            $enderecos = $this->enderecoService->recuperarEnderecos($idCliente);
            $retorno->setData($enderecos);
            return $this->ResponseCustom($retorno->getObjeto());

        } catch (\Exception $e) {
            $retorno->setErro(1);
            $retorno->setMensagem($e->getMessage());
            return $this->ResponseCustom($retorno->getObjeto(), 400);
        }
    }

    public function removerEndereco($id)
    {
        $retorno = new RetornoPadrao();
        try {
            $endereco = $this->enderecoService->removerEndereco($id);
            $retorno->setData($endereco);
            return $this->ResponseCustom($retorno->getObjeto());

        } catch (\Exception $e) {
            $retorno->setErro(1);
            $retorno->setMensagem($e->getMessage());
            return $this->ResponseCustom($retorno->getObjeto(), 400);
        }
    }

    public function cadastrar(Request $request)
    {
        $retorno = new RetornoPadrao();
        try {
            $idImagem = null;
            $data = $request->all();

            $this->enderecoService->setEndereco($data);
            $this->enderecoService->validarEndereco();

            $endereco = $this->enderecoService->salvar();

            $retorno->setData($endereco->toArray());

            return $this->ResponseCustom($retorno->getObjeto());

        } catch (\Exception $e) {
            $retorno->setErro(1);
            $retorno->setMensagem($e->getMessage());
            return $this->ResponseCustom($retorno->getObjeto(), 400);
        }
    }
}