<?php

namespace App\Http\Controllers\Core;
use App\Http\Controllers\Controller;
use App\Http\Services\ClienteService;
use Illuminate\Http\Request;

use App\Http\Retornos\RetornoPadrao;


class ClienteCoreController extends Controller
{
    protected $usuario;
    protected $clienteService;

    /**
     * ClienteController constructor.
     */
    public function __construct()
    {
        $this->clienteService = new ClienteService();

    }

    public function consultar()
    {
        $retorno = new RetornoPadrao();
        try {

            $clientes = $this->clienteService->consultar();
            $retorno->setData($clientes->toArray());

            
            return $this->ResponseCustom($retorno->getObjeto());

        } catch (\Exception $e) {
            $retorno->setErro(1);
            $retorno->setMensagem($e->getMessage());
            return $this->ResponseCustom($retorno->getObjeto(), 400);
        }
    }

    public function consultarClientePorId($id)
    {
        $retorno = new RetornoPadrao();
        try {

            $cliente = $this->clienteService->consultarClientePorId($id);
            $retorno->setData($cliente->toArray());


            return $this->ResponseCustom($retorno->getObjeto());

        } catch (\Exception $e) {
            $retorno->setErro(1);
            $retorno->setMensagem($e->getMessage());
            return $this->ResponseCustom($retorno->getObjeto(), 400);
        }
    }

    public function removerCliente($id)
    {
        $retorno = new RetornoPadrao();
        try {

            $cliente = $this->clienteService->removerClientePorId($id);
            $retorno->setData($cliente->toArray());


            return $this->ResponseCustom($retorno->getObjeto());

        } catch (\Exception $e) {
            $retorno->setErro(1);
            $retorno->setMensagem($e->getMessage());
            return $this->ResponseCustom($retorno->getObjeto(), 400);
        }
    }
}