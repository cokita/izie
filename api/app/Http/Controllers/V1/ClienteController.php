<?php

namespace App\Http\Controllers\V1;
use App\Http\Controllers\Core\ClienteCoreController;
use App\Http\Services\ImagemService;
use Illuminate\Http\Request;

use App\Http\Retornos\RetornoPadrao;


class ClienteController extends ClienteCoreController
{

    public function __construct()
    {
        parent::__construct();
    }

    public function cadastrar(Request $request)
    {
        $retorno = new RetornoPadrao();
        try {
            $idImagem = null;
            $data = $request->all();

            $this->clienteService->setCliente($data);
            $this->clienteService->validarCliente();

            if(isset($data['image']) && $data['image'] != ""){
                $imagemService = new ImagemService();
                $imagem = $imagemService->salvar($data['image']);
                $idImagem = $imagem->Id;
                unset($data['image']);
            }

            $cliente = $this->clienteService->salvar($idImagem);

            $retorno->setData($cliente->toArray());
            
            return $this->ResponseCustom($retorno->getObjeto());

        } catch (\Exception $e) {
            $retorno->setErro(1);
            $retorno->setMensagem($e->getMessage());
            return $this->ResponseCustom($retorno->getObjeto(), 400);
        }
    }

}