<?php

namespace App\Http\Retornos;

class RetornoPadrao
{
    private $codigo = null;

    private $data = null;

    private $mensagem = null;

    private $erro = null;

    public function __construct()
    {
        $this->erro = 0;
        $this->mensagem = "Sucesso!";
    }

    public function setData($data)
    {
        $this->data = $data;
        return $this->data;
    }

    public function setMensagem($mensagem)
    {
        $this->mensagem = $mensagem;
        return $this->mensagem;
    }

    public function setCodigo($codigo)
    {
        $this->codigo = $codigo;
        return $this->codigo;
    }

    public function setErro($erro)
    {
        $this->erro = $erro;
        return $this->erro;
    }


    public function getData()
    {
        return $this->data;
    }

    public function getMensagem()
    {
        return $this->mensagem;
    }

    public function getCodigo()
    {
        return $this->codigo;
    }

    public function getErro()
    {
        return $this->erro;
    }

    public function getObjeto()
    {
        $array = array(
            'codigo' => $this->codigo,
            'mensagem' => $this->mensagem,
            'data' => $this->data,
            'erro' => $this->erro
        );

        return $array;
    }

}