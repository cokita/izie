<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 23 Apr 2017 13:16:16 -0300.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Cliente
 * 
 * @property int $Id
 * @property int $IdImagem
 * @property string $Nome
 * @property \Carbon\Carbon $DataNascimento
 * @property string $Sexo
 * @property string $CPF
 * @property string $Email
 * @property string $Ativo
 * @property \Carbon\Carbon $DataCriacao
 * 
 * @property \App\Models\Imagem $imagem
 * @property \Illuminate\Database\Eloquent\Collection $enderecos
 *
 * @package App\Models
 */
class Cliente extends Eloquent
{
	protected $table = 'Cliente';
	protected $primaryKey = 'Id';
	public $timestamps = false;

	protected $casts = [
		'IdImagem' => 'int'
	];

	protected $dates = [
		'DataNascimento',
		'DataCriacao'
	];

	protected $fillable = [
		'IdImagem',
		'Nome',
		'DataNascimento',
		'Sexo',
		'CPF',
		'Email',
		'Ativo',
		'DataCriacao'
	];

	public function imagem()
	{
		return $this->belongsTo(\App\Models\Imagem::class, 'IdImagem');
	}

	public function enderecos()
	{
		return $this->hasMany(\App\Models\Endereco::class, 'IdCliente');
	}
}
