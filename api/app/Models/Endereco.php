<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 23 Apr 2017 13:16:16 -0300.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Endereco
 * 
 * @property int $Id
 * @property int $IdCliente
 * @property int $IdUf
 * @property string $Nome
 * @property string $CEP
 * @property string $Logradouro
 * @property string $Complemento
 * @property string $Bairro
 * @property string $Cidade
 * @property string $Ativo
 * @property \Carbon\Carbon $DataCriacao
 * 
 * @property \App\Models\Cliente $cliente
 * @property \App\Models\Uf $uf
 *
 * @package App\Models
 */
class Endereco extends Eloquent
{
	protected $table = 'Endereco';
	protected $primaryKey = 'Id';
	public $timestamps = false;

	protected $casts = [
		'IdCliente' => 'int',
		'IdUf' => 'int'
	];

	protected $dates = [
		'DataCriacao'
	];

	protected $fillable = [
		'IdCliente',
		'IdUf',
		'Nome',
		'CEP',
		'Logradouro',
		'Complemento',
		'Bairro',
		'Cidade',
		'Ativo',
		'DataCriacao'
	];

	public function cliente()
	{
		return $this->belongsTo(\App\Models\Cliente::class, 'IdCliente');
	}

	public function uf()
	{
		return $this->belongsTo(\App\Models\Uf::class, 'IdUf');
	}
}
