<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 23 Apr 2017 13:16:16 -0300.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Uf
 * 
 * @property int $Id
 * @property string $Nome
 * @property string $Sigla
 * @property string $Ativo
 * 
 * @property \Illuminate\Database\Eloquent\Collection $enderecos
 *
 * @package App\Models
 */
class Uf extends Eloquent
{
	protected $table = 'Uf';
	protected $primaryKey = 'Id';
	public $timestamps = false;

	protected $fillable = [
		'Nome',
		'Sigla',
		'Ativo'
	];

	public function enderecos()
	{
		return $this->hasMany(\App\Models\Endereco::class, 'IdUf');
	}
}
