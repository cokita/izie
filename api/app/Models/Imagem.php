<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 23 Apr 2017 13:16:16 -0300.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Imagem
 * 
 * @property int $Id
 * @property string $NomeArquivado
 * @property string $Path
 * @property string $Base64
 * @property string $Mimetype
 * @property string $Ativo
 * @property \Carbon\Carbon $DataCriacao
 * 
 * @property \Illuminate\Database\Eloquent\Collection $clientes
 *
 * @package App\Models
 */
class Imagem extends Eloquent
{
	protected $table = 'Imagem';
	protected $primaryKey = 'Id';
	public $timestamps = false;

	protected $dates = [
		'DataCriacao'
	];

	protected $fillable = [
		'NomeArquivado',
		'Path',
		'Base64',
		'Mimetype',
		'Ativo',
		'DataCriacao'
	];

	public function clientes()
	{
		return $this->hasMany(\App\Models\Cliente::class, 'IdImagem');
	}
}
