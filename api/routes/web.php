<?php
/*header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: Authorization, Content-Type' );*/
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/401', function () {
    return view('errors/401');
});

Route::group(['middleware' => 'verificar', 'prefix' => 'Core'], function () {
	Route::get('/', function(){ return view('welcome'); });
	Route::get('/consultarTodasUfs', 'Core\UfController@consultarTodas');

	Route::group(['prefix' => 'Cliente'], function () {
         Route::get('/consultar', 'Core\ClienteCoreController@consultar');
         Route::get('/consultar-cliente-por-id/{id}', 'Core\ClienteCoreController@consultarClientePorId');
         Route::get('/remover-cliente/{id}', 'Core\ClienteCoreController@removerCliente');
    });

    Route::group(['prefix' => 'Endereco'], function () {
        Route::get('/consultar-enderecos-por-cliente/{idCliente}', 'Core\EnderecoController@consultarEnderecosPorCliente');
        Route::get('/remover-endereco/{id}', 'Core\EnderecoController@removerEndereco');
        Route::post('/cadastrar', 'Core\EnderecoController@cadastrar');
    });
});
Route::group(['middleware' => 'verificar', 'prefix' => 'V1'], function () {
	Route::get('/', function(){ return view('welcome'); });
	Route::group(['prefix' => 'Cliente'], function () {
    	Route::post('/cadastrar', 'V1\ClienteController@cadastrar');
            
    });
});


Route::group(['middleware' => 'verificar', 'prefix' => 'V2'], function () {
    Route::get('/', function(){ return view('welcome'); });
    Route::group(['prefix' => 'Cliente'], function () {
        Route::post('/cadastrar', 'V2\ClienteController@cadastrar');

    });
});

Route::group(['middleware' => 'verificar', 'prefix' => 'V3'], function () {
    Route::get('/', function(){ return view('welcome'); });
    Route::group(['prefix' => 'Cliente'], function () {
        Route::post('/cadastrar', 'V3\ClienteController@cadastrar');

    });
});