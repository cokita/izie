CREATE SCHEMA `izie` DEFAULT CHARACTER SET utf8 ;

CREATE TABLE `izie`.`Uf` (
  `Id` INT NOT NULL AUTO_INCREMENT,
  `Nome` VARCHAR(150) NOT NULL,
  `Sigla` VARCHAR(2) NOT NULL,
  `Ativo` CHAR(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`Id`));

INSERT INTO `izie`.`Uf` (`Nome`, `Sigla`) VALUES ('Acre', 'AC');
INSERT INTO `izie`.`Uf` (`Nome`, `Sigla`) VALUES ('Alagoas', 'AL');
INSERT INTO `izie`.`Uf` (`Nome`, `Sigla`) VALUES ('Amapá', 'AP');
INSERT INTO `izie`.`Uf` (`Nome`, `Sigla`) VALUES ('Amazonas', 'AM');
INSERT INTO `izie`.`Uf` (`Nome`, `Sigla`) VALUES ('Bahia', 'BA');
INSERT INTO `izie`.`Uf` (`Nome`, `Sigla`) VALUES ('Ceará', 'CE');
INSERT INTO `izie`.`Uf` (`Nome`, `Sigla`) VALUES ('Distrito Federal', 'DF');
INSERT INTO `izie`.`Uf` (`Nome`, `Sigla`) VALUES ('Espírito Santo', 'ES');
INSERT INTO `izie`.`Uf` (`Nome`, `Sigla`) VALUES ('Goiás', 'GO');
INSERT INTO `izie`.`Uf` (`Nome`, `Sigla`) VALUES ('Maranhão', 'MA');
INSERT INTO `izie`.`Uf` (`Nome`, `Sigla`) VALUES ('Mato Grosso', 'MT');
INSERT INTO `izie`.`Uf` (`Nome`, `Sigla`) VALUES ('Mato Grosso do Sul', 'MS');
INSERT INTO `izie`.`Uf` (`Nome`, `Sigla`) VALUES ('Minas Gerais', 'MG');
INSERT INTO `izie`.`Uf` (`Nome`, `Sigla`) VALUES ('Pará', 'PA');
INSERT INTO `izie`.`Uf` (`Nome`, `Sigla`) VALUES ('Paraíba', 'PB');
INSERT INTO `izie`.`Uf` (`Nome`, `Sigla`) VALUES ('Paraná', 'PR');
INSERT INTO `izie`.`Uf` (`Nome`, `Sigla`) VALUES ('Pernambuco', 'PE');
INSERT INTO `izie`.`Uf` (`Nome`, `Sigla`) VALUES ('Piauí', 'PI');
INSERT INTO `izie`.`Uf` (`Nome`, `Sigla`) VALUES ('Rio de Janeiro', 'RJ');
INSERT INTO `izie`.`Uf` (`Nome`, `Sigla`) VALUES ('Rio Grande do Norte', 'RN');
INSERT INTO `izie`.`Uf` (`Nome`, `Sigla`) VALUES ('Rio Grande do Sul', 'RS');
INSERT INTO `izie`.`Uf` (`Nome`, `Sigla`) VALUES ('Rondônia', 'RO');
INSERT INTO `izie`.`Uf` (`Nome`, `Sigla`) VALUES ('Roraíma', 'RR');
INSERT INTO `izie`.`Uf` (`Nome`, `Sigla`) VALUES ('Santa Catarina', 'SC');
INSERT INTO `izie`.`Uf` (`Nome`, `Sigla`) VALUES ('São Paulo', 'SP');
INSERT INTO `izie`.`Uf` (`Nome`, `Sigla`) VALUES ('Sergipe', 'SE');
INSERT INTO `izie`.`Uf` (`Nome`, `Sigla`) VALUES ('Tocantins', 'TO');

CREATE TABLE `izie`.`Cliente` (
  `Id` INT NOT NULL AUTO_INCREMENT,
  `IdImagem` INT NULL,
  `Nome` VARCHAR(255) NOT NULL,
  `DataNascimento` DATE NOT NULL,
  `Sexo` CHAR(1) NOT NULL,
  `CPF` VARCHAR(11) NULL,
  `Email` VARCHAR(255) NULL,
  `Ativo` CHAR(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`Id`));


CREATE TABLE `izie`.`Imagem` (
  `Id` INT NOT NULL AUTO_INCREMENT,
  `NomeArquivado` VARCHAR(255) NOT NULL,
  `Path` VARCHAR(255) NOT NULL,
  `Mimetype` VARCHAR(45) NULL,
  `Ativo` VARCHAR(45) NULL,
  PRIMARY KEY (`Id`));

ALTER TABLE `izie`.`Imagem`
CHANGE COLUMN `Ativo` `Ativo` CHAR(1) NULL DEFAULT '1' ;


ALTER TABLE `izie`.`Cliente`
ADD INDEX `FK_Cliente_Imagem_idx` (`IdImagem` ASC);
ALTER TABLE `izie`.`Cliente`
ADD CONSTRAINT `FK_Cliente_Imagem`
  FOREIGN KEY (`IdImagem`)
  REFERENCES `izie`.`Imagem` (`Id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

CREATE TABLE `izie`.`Endereco` (
  `Id` INT NOT NULL AUTO_INCREMENT,
  `IdCliente` INT NOT NULL,
  `IdUf` INT NOT NULL,
  `Nome` VARCHAR(45) NULL,
  `CEP` VARCHAR(20) NOT NULL,
  `Logradouro` VARCHAR(255) NOT NULL,
  `Complemento` VARCHAR(255) NULL,
  `Bairro` VARCHAR(150) NOT NULL,
  `Cidade` VARCHAR(150) NULL,
  PRIMARY KEY (`Id`),
  INDEX `FK_ENDERECO_CLIENTE_idx` (`IdCliente` ASC),
  INDEX `FK_ENDERECO_UF_idx` (`IdUf` ASC),
  CONSTRAINT `FK_ENDERECO_CLIENTE`
    FOREIGN KEY (`IdCliente`)
    REFERENCES `izie`.`Cliente` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `FK_ENDERECO_UF`
    FOREIGN KEY (`IdUf`)
    REFERENCES `izie`.`Uf` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

ALTER TABLE `izie`.`Cliente`
ADD COLUMN `DataCriacao` DATETIME NULL DEFAULT CURRENT_TIMESTAMP AFTER `Ativo`;

ALTER TABLE `izie`.`Endereco`
ADD COLUMN `Ativo` CHAR(1) NULL DEFAULT '1' AFTER `Cidade`,
ADD COLUMN `DataCriacao` DATETIME NULL DEFAULT CURRENT_TIMESTAMP AFTER `Ativo`;
ALTER TABLE `izie`.`Imagem`
ADD COLUMN `DataCriacao` DATETIME NULL DEFAULT CURRENT_TIMESTAMP AFTER `Ativo`;


ALTER TABLE `izie`.`Imagem`
CHANGE COLUMN `NomeArquivado` `NomeArquivado` VARCHAR(255) NULL ,
CHANGE COLUMN `Path` `Path` VARCHAR(255) NULL ,
CHANGE COLUMN `Mimetype` `Mimetype` VARCHAR(45) NULL ,
CHANGE COLUMN `Ativo` `Ativo` CHAR(1) NOT NULL DEFAULT '1' ,
CHANGE COLUMN `DataCriacao` `DataCriacao` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ;


