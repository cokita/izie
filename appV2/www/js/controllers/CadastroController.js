controllers.controller('CadastroController', function ($scope, Clientes, Camera, $cordovaToast) {
    $scope.cliente = {};

    $scope.modalShown = false;
    $scope.toggleModal = function () {
        $scope.modalShown = !$scope.modalShown;
    };

    $scope.salvar = function (form) {
        if (form.$valid) {

            if ($scope.lastPhoto) {
                Camera.toBase64Image($scope.lastPhoto).then(function (_result) {
                    console.log(_result);
                    $scope.cliente.image = "data:image/jpeg;base64," + _result.imageData;
                    $scope.salvarCliente(form);
                });
            }else{
                $scope.salvarCliente(form);
            }

        }else{
            $scope.validar(form);
            console.log('dados inválidos...');
        }

    };

    $scope.salvarCliente = function(form)
    {
        Clientes.salvar($scope.cliente).then(function (result) {
            $cordovaToast.show("Dados Salvos com sucesso!", 'short', 'center');
            $scope.limpar(form);
        }, function (error) {
            console.log(error);
        });
    };

    $scope.validar = function(form){
        if(form.nome.$error.required){
            $cordovaToast.show("O Nome é obrigatório.", 'short', 'center');
        }else if(form.dataNascimento.$error.required){
            $cordovaToast.show("A data de nascimento é obrigatória.", 'short', 'center');
        }else if(form.cpf.$error.required){
            $cordovaToast.show("O CPF é obrigatório.", 'short', 'center');
        }else if(form.email.$error.required){
            $cordovaToast.show("O e-mail é obrigatório.", 'short', 'center');
        }else if(form.email.$error.email){
            $cordovaToast.show("O e-mail está inválido.", 'short', 'center');
        }else if(angular.isUndefined($scope.cliente.Sexo) || $scope.cliente.Sexo == ""){
            $cordovaToast.show("Favor informe o sexo.", 'short', 'center');
        }
    };

    $scope.limpar = function (form) {
        $scope.lastPhoto = false;
        $scope.cliente = {};
        // zerar o form ao seu estado inicial.
        form.$setPristine();
    };

    $scope.makeThumbNail = function () {

        if (!$scope.lastPhoto) {
            alert("Missing Photo");
            return;
        }

        Camera.resizeImage($scope.lastPhoto).then(function (_result) {
            $scope.thumb = "data:image/jpeg;base64," + _result.imageData;
        }, function (_error) {
            console.log(_error);
        });
    };

    $scope.getPhoto = function () {
        var options = {
            'buttonLabels': ['Tirar Foto', 'Escolher da Galeria'],
            'addCancelButtonWithLabel': 'Cancelar'
        };
        window.plugins.actionsheet.show(options, callback);
    };

    function callback(buttonIndex) {
        if (buttonIndex === 1) {

            var picOptions = {
                destinationType: navigator.camera.DestinationType.FILE_URI,
                quality: 75,
                targetWidth: 500,
                targetHeight: 500,
                allowEdit: true,
                saveToPhotoAlbum: false
            };

            Camera.getPicture(picOptions).then(function (imageURI) {
                $scope.lastPhoto = imageURI;
                $scope.newPhoto = true;

            }, function (err) {
                $scope.newPhoto = false;
            });
        } else if (buttonIndex === 2) {

            var picOptions = {
                destinationType: navigator.camera.DestinationType.FILE_URI,
                quality: 75,
                targetWidth: 500,
                targetHeight: 500,
                sourceType: navigator.camera.PictureSourceType.PHOTOLIBRARY
            };


            Camera.getPictureFromGallery(picOptions).then(function (imageURI) {
                $scope.lastPhoto = imageURI;
                $scope.newPhoto = true;

            }, function (err) {
                console.log(err);
                $scope.newPhoto = false;
            });
        }

    };

});