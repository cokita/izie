app.config(function($stateProvider, $urlRouterProvider, $httpProvider) {

    $httpProvider.interceptors.push(function($rootScope, $cordovaToast){
        return{
            request: function(config){
                $rootScope.$broadcast("LoadingIzie:exibe");
                return config;
            },
            response: function(response){
                $rootScope.$broadcast("LoadingIzie:esconde");
                return response;
            },
            responseError: function (rejection) {
                $rootScope.$broadcast("LoadingIzie:esconde");
                $cordovaToast.show(rejection.data.mensagem, 'short', 'center');

                throw rejection;
            }
        }
    });

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider

  // setup an abstract state for the tabs directive
    .state('tab', {
    url: '/tab',
    abstract: true,
    templateUrl: 'templates/tabs.html'
  })

  // Each tab has its own nav history stack:

  .state('tab.sobre', {
    url: '/sobre',
    views: {
      'tab-sobre': {
        templateUrl: 'templates/tab-sobre.html',
        controller: 'SobreController'
      }
    }
  })

  .state('tab.lista', {
      url: '/lista',
      views: {
        'tab-lista': {
          templateUrl: 'templates/tab-lista.html',
          controller: 'ListaController'
        }
      }
    })
    .state('tab.lista-detail', {
      url: '/lista/:id',
      views: {
        'tab-lista': {
          templateUrl: 'templates/lista-detail.html',
          controller: 'ListaDetalheController'
        }
      }
    })
    .state('tab.cadastro', {
      url: '/cadastro',
      views: {
        'tab-cadastro': {
          templateUrl: 'templates/tab-cadastro.html',
          controller: 'CadastroController'
        }
      }
    });


  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/tab/sobre');

});
