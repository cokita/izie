filters.filter('dateInFormat', function ($filter) {

    return function (input, format) {
        if(!angular.isUndefined(input)) {
            var data = new Date(input);
            if (angular.isDate(data)) {
                return $filter('date')(data, format);
            } else {
                return "data inválida";
            }
        }
    }
});