services.factory('Clientes', function ($http, $q, WebService) {
	
	function Clientes()
	{
		this.consultarClientePorId = function(id){
            return WebService.get({method: 'Core/Cliente/consultar-cliente-por-id/'+id});
		};
		this.todos = function(){
			return WebService.get({method: 'Core/Cliente/consultar'});
		};
		this.salvar = function(Cliente){
			return WebService.post({method:'V1/Cliente/cadastrar', data: Cliente});
		};
		this.remover = function(id){
			return WebService.get({method: 'Core/Cliente/remover-cliente/'+ id})
		}
	}

	return new Clientes();

});