services.factory('WebService', function ($http, $q, EnderecoAPI) {

	return {
        get: function (configs) {

            var deferred = $q.defer(), defaults = {
                url: EnderecoAPI.url,
                method: "",
                headers: {
                     "Authorization": "Basic " + "apiKeyHere"
                },
                ignoreError: false,
                data: null,
                cache: false
            };

            defaults = angular.extend(defaults, configs);

            $http.get(
                defaults.url + defaults.method, { 
                params: defaults.data,
                headers: defaults.headers
            })
            .success(function(data) {
                deferred.resolve(data);
            })
            .error(function(data) {
                deferred.reject(data);
            });

            return deferred.promise;
        },
        post: function (configs) {
            var deferred = $q.defer(), defaults = {
                url: EnderecoAPI.url,
                method: "",
                headers: {
                     "Authorization": "Basic " + "apiKeyHere"
                },
                ignoreError: false,
                data: null,
                cache: false
            };

            defaults = angular.extend(defaults, configs);

            $http({
                method: 'POST',
                url: defaults.url + defaults.method,
                data: defaults.data,
                headers: defaults.headers,
                cache: defaults.cache
            }).success(function (data, status, headers, config) {
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
            	deferred.reject(data);
            });

            return deferred.promise;
        }
    }
});