var app = angular.module('izie', ['ionic', 'ui.mask', 'ngCordova',
    'izie.controllers', 'izie.services','izie.factories', 'izie.filters'])

var controllers = angular.module('izie.controllers', []);
var services = angular.module('izie.services', []);
var factories = angular.module('izie.factories', []);
var filters = angular.module('izie.filters', []);
//var directives = angular.module('izie.directives', []);
//var providers = angular.module('izie.providers', []);

app.run(function($ionicPlatform, $rootScope, $ionicLoading) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }

      $rootScope.$on('LoadingIzie:exibe', function(){
          $ionicLoading.show();
      });

      $rootScope.$on('LoadingIzie:esconde', function(){
          $ionicLoading.hide();
      });
  });
})

app.constant('EnderecoAPI', {
  url: 'http://192.168.56.1/'
  //url : 'http://izie.api.com/'
  //url : '/api/'
});
