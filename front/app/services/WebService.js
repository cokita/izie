services.factory('WebService', function ($http, $q, EnderecoAPI) {

    return {
        get: function (configs) {

            var deferred = $q.defer(), defaults = {
                url: EnderecoAPI.url,
                method: "",
                headers: {
                    //'Authorization': null
                     "Authorization": "Basic " + "apiKeyHere",
                     "Content-Type" : "application/json"
                },
                ignoreError: false,
                data: null,
                cache: false
            };

            defaults = angular.extend(defaults, configs);

            $http({
                method: 'GET',
                url: defaults.url + defaults.method,
                data: defaults.data,
                headers: defaults.headers,
                cache: defaults.cache,
            }).then(function successCallback(response) {
                deferred.resolve(response);
            }, function errorCallback(response) {
                deferred.reject(response);
            });

            return deferred.promise;
        },
        post: function (configs) {
            var deferred = $q.defer(), defaults = {
                url: EnderecoAPI.url,
                method: "",
                headers: {
                    //'Authorization': null
                },
                ignoreError: false,
                data: null,
                cache: false
            };

            defaults = angular.extend(defaults, configs);

            $http({
                method: 'POST',
                url: defaults.url + defaults.method,
                data: defaults.data,
                headers: defaults.headers,
                cache: defaults.cache
            }).then(function successCallback(response) {
                deferred.resolve(response);
            }, function errorCallback(response) {
                deferred.reject(response);
            });

            return deferred.promise;
        }
    }
});