app.config(function ($stateProvider, $urlRouterProvider, $locationProvider) {

    $urlRouterProvider.otherwise('/app/home');

    $stateProvider

        .state('app', {
            url: '/app',
            abstract: true,
            controller: 'MainController',
            templateUrl: '/app/templates/main.html'
        })
        .state('app.home', {
            url: '/home',
            title: "Bem Vindo!",
            views: {
                'app-view': {
                    templateUrl: "/app/templates/home.html",
                    controller: 'HomeController'
                }
            }
        })
        .state('app.about', {
            url: '/about',
            title: "Sobre a Ana",
            views: {
                'app-view': {
                    templateUrl: "/app/templates/about.html",
                    controller: 'AboutController'
                }
            }
        })
        /*.state("login", {
            url: "/login",
            title: "Faça login para acessar.",
            templateUrl: "templates/login.html",
            controller: "LoginController",
            cache: true
        })
        .state('app.logout', {
            url: '/logout',
            onEnter: function (user) {
                user.logout();
            }
        })
        .state("app.permissao401", {
            url: "/permissao/401",
            title: "Acesso negado!",
            views: {
                'app-view': {
                    templateUrl: "/templates/permissao-401.html"
                }
            }
        })
        .state("app.permissao404", {
            url: "/permissao/404",
            title: "Página não encontrada!",
            views: {
                'app-view': {
                    templateUrl: "/templates/permissao-404.html"
                }
            }
        })*/
});
