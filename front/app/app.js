var factories = angular.module('app.factories', []);
var controllers = angular.module('app.controllers', []);
var directives = angular.module('app.directives', []);
var filters = angular.module('app.filters', []);
var services = angular.module('app.services', []);
var constants = angular.module('app.constants', []);

var app = angular.module("app", ["ui.router", "app.controllers", "app.services"]);

app.run(function ($rootScope, $location) {

    console.log('to no app');
    
});

app.constant('EnderecoAPI', {
  url : 'http://izie.api.com/V1/'
  //url : '/api/v1/'
});


app.config(function ($locationProvider, $httpProvider, $qProvider) {
	$qProvider.errorOnUnhandledRejections(false);
});