directives.directive('cadastrarEndereco', ['$document', '$ionicModal', 'Uf', '$cordovaToast', 'Endereco',
    function ($document, $ionicModal, Uf, $cordovaToast, Endereco) {
    return {
        restrict: 'E',
        scope: true,
        template: '<button type="button" class="button button-block button-royal icon-right ion-ios-home"> Cadastrar Endereço </button>',
        link: function (scope, elem, attrs) {
            elem.on('click', function () {
                scope.endereco = {};
                Uf.consultarUfs().then(function(result){
                    scope.ufs = result.data;
                    $ionicModal.fromTemplateUrl('templates/modal/endereco.html', {
                        scope: scope,
                        animation: 'slide-in-up',
                        focusFirstInput: true
                    }).then(function(modal) {
                        scope.modal = modal;
                        scope.modal.show();
                        scope.addEndereco = function () {
                            var validacao = scope.validarEndereco(scope.endereco);
                            if(validacao){
                                console.log(scope.cliente.Id);
                                console.log(angular.isUndefined(scope.cliente));
                                if(angular.isUndefined(scope.cliente) ||
                                    angular.isUndefined(scope.cliente.Id) || scope.cliente.Id === ""){
                                    scope.enderecos.push(scope.endereco);
                                    scope.modal.remove();
                                }else {
                                    scope.endereco.IdCliente = scope.cliente.Id;
                                    Endereco.salvar(scope.endereco).then(function () {
                                        scope.modal.remove();
                                        scope.atualizar();
                                    });
                                }
                            }
                        };
                    });
                });
            });

            scope.validarEndereco = function (endereco) {
                var retorno = true;
                if(angular.isUndefined(endereco.Nome) || endereco.Nome == "") {
                    $cordovaToast.show("Favor preencher o NOME (identificador) do endereço.", 'short', 'center');
                    retorno = false;
                }else if(angular.isUndefined(endereco.IdUf) || endereco.IdUf == ""){
                    $cordovaToast.show("Favor preencher a UF.", 'short', 'center');
                    retorno = false;
                } else if(angular.isUndefined(endereco.Logradouro) || endereco.Logradouro == ""){
                    $cordovaToast.show("Favor preencher o Logradouro.", 'short', 'center');
                    retorno = false;
                }else if(angular.isUndefined(endereco.Bairro) || endereco.Bairro == ""){
                    $cordovaToast.show("Favor preencher o Bairro.", 'short', 'center');
                    retorno = false;
                } else if(angular.isUndefined(endereco.Cidade) || endereco.Cidade == ""){
                    $cordovaToast.show("Favor preencher o Cidade.", 'short', 'center');
                    retorno = false;
                } else if(angular.isUndefined(endereco.CEP) || endereco.CEP == "") {
                    $cordovaToast.show("Favor preencher o CEP.", 'short', 'center');
                    retorno = false;
                }
                return retorno;
            }

            scope.openModal = function() {
                scope.modal.show();
            };

            scope.closeModal = function() {
                scope.modal.hide();
            };


            scope.$on('$destroy', function() {
                scope.modal.remove();
            });

            scope.$on('modal.hidden', function() {
                // Execute action
            });

            scope.$on('modal.removed', function() {
                // Execute action
            });
        }
    };
}]);