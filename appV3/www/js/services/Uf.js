services.service('Uf', function ($http, $q, WebService) {

    return {
        consultarUfs: function () {
            return WebService.get({method: 'Core/consultarTodasUfs'});
        }
    };

});