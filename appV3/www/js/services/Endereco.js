services.service('Endereco', function ($http, $q, WebService) {

    return {
        remover : function(id){
            return WebService.get({method: 'Core/Endereco/remover-endereco/'+ id})
        },
        salvar: function (Endereco) {
            return WebService.post({method: "Core/Endereco/cadastrar", data: Endereco});
        }
    };

});