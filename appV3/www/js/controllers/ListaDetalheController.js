controllers.controller('ListaDetalheController', function($scope, $state, Clientes, Endereco){

    $scope.atualizar = function() {
        Clientes.consultarClientePorId($state.params.id).then(function (resultado) {
            console.log(resultado);
            $scope.cliente = resultado.data;
        }, function (erro) {
            console.log(erro);
        });
    };

    $scope.atualizar();

    $scope.remover = function (id) {
        Endereco.remover(id).then(function () {
            $scope.atualizar();
        })

    }

});