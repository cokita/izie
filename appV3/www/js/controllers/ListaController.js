controllers.controller('ListaController', function($scope, Clientes){
	
	$scope.atualizar = function(){
		$scope.registros = false;
		Clientes.todos().then(function(resultado){
			console.log(resultado);
			$scope.clientes = resultado.data;
			/*$scope.registros = resultado.usuarios;*/
			$scope.$broadcast('scroll.refreshComplete');
		}, function(erro){
			console.log(erro);
		});
	};

	$scope.atualizar();

	$scope.remover = function (id) {
		Clientes.remover(id).then(function () {
			$scope.atualizar();
        });
    }


});